/*
 * Copyright (c) 2018 Brendan Szymanski
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.echo

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.opus.echo.database.ServerDatabase
import com.opus.echo.irc.Server
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.home_layout.*
import kotlin.concurrent.thread

class HomeActivity : AppCompatActivity(), ServerDatabase.ServerAddListener {
    // Server and channel objects
    private var servers = mutableListOf<Server>()
    private var serverDatabase: ServerDatabase? = null
    private var serverNames = mutableListOf<String>()

    // Add channel dialog layout components
    private lateinit var addChannelLayout: View
    private lateinit var serverSpinner: Spinner
    private lateinit var addServerButton: RelativeLayout
    private lateinit var serverSpinnerAdapter: ArrayAdapter<String>

    // Add server dialog layout components
    private lateinit var addServerLayout: View
    private lateinit var serverName: EditText
    private lateinit var serverIp: EditText
    private lateinit var serverPort: EditText
    private lateinit var serverNickname: EditText
    private lateinit var serverRealName: EditText
    private lateinit var serverPassword: EditText

    // The onClick listener that opens the new server dialog.
    private val addServerClickListener = View.OnClickListener {
        val addServerLayoutParent = addServerLayout.parent
        if (addServerLayoutParent != null) {
            (addServerLayoutParent as ViewGroup).removeAllViews()
        }

        AlertDialog.Builder(this)
            .setTitle("New Server")
            .setPositiveButton("Add") { _, _ ->
                onServerDialogAdd()
            }
            .setNegativeButton("Cancel", null)
            .setView(addServerLayout)
            .create().show()
    }

    private val addChannelClickListener = View.OnClickListener {
        val addChannelLayoutParent = addChannelLayout.parent
        if (addChannelLayoutParent != null) {
            (addChannelLayoutParent as ViewGroup).removeAllViews()
        }

        AlertDialog.Builder(this)
            .setTitle("New Channel")
            .setPositiveButton("Add", null)
            .setNegativeButton("Cancel", null)
            .setView(addChannelLayout)
            .create().show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_layout)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        serverDatabase = ServerDatabase.getInstance(this)
        serverDatabase?.serverAddListeners!!.add(this)

        configureServerList()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onServerAdded(server: Server) {
        Log.i(TAG, "Server Added: ${server.name}, ${server.address}, ${server.port}, ${server.nickname}")
        servers.add(server)
        serverNames.add(server.name)
        runOnUiThread {
            updateServerListVisibility()
            updateServerSpinnerVisibility()
            server_list.adapter?.notifyDataSetChanged()
            serverSpinnerAdapter.notifyDataSetChanged()
        }
    }

    private fun setup() {
        updateServerListVisibility()
        for (server in servers) {
            Log.i(TAG, "${server.name} added")
            serverNames.add(server.name)
        }
        setupViews()
        updateServerSpinnerVisibility()
        add.setOnClickListener(addChannelClickListener)
    }

    private fun updateServerListVisibility() {
        Log.i(TAG, servers.isNotEmpty().toString())
        if (servers.isNotEmpty()) {
            content_home_container.visibility = View.VISIBLE
            content_home_placeholder_container.visibility = View.GONE
        } else {
            content_home_container.visibility = View.GONE
            content_home_placeholder_container.visibility = View.VISIBLE
        }
    }

    private fun updateServerSpinnerVisibility() {
        Log.i("server spinner", "${serverNames.size}")
        if (serverNames.size == 0) {
            serverSpinner.visibility = View.GONE
        } else {
            serverSpinner.visibility = View.VISIBLE
        }
    }

    private fun configureServerList() {
        thread {
            servers.addAll(serverDatabase?.serverDao()!!.getServers())
            runOnUiThread {
                server_list.layoutManager = LinearLayoutManager(this)
                server_list.isNestedScrollingEnabled = false
                server_list.adapter = ServerListAdapter(this, servers)

                setup()
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun setupViews() {
        // Setup add channel dialog views.
        addChannelLayout = layoutInflater.inflate(R.layout.add_channel_dialog_content, null)
        addServerButton = addChannelLayout.findViewById(R.id.add_server)
        addServerButton.setOnClickListener(addServerClickListener)
        serverSpinner = addChannelLayout.findViewById(R.id.server_spinner)
        serverSpinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, serverNames)
        serverSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        serverSpinner.adapter = serverSpinnerAdapter
        // Add an onClick listener to the advanced dropdown container so when clicked, advanced options are
        // expanded and shown. No point in creating objects for the views here, since they're only
        // used once and would just waste memory.
        addChannelLayout.findViewById<RelativeLayout>(R.id.add_advanced_options)!!.setOnClickListener {
            addChannelLayout.findViewById<ImageView>(R.id.add_advanced_dropdown)!!.rotation += 180f
            val advancedOptions = addChannelLayout.findViewById<LinearLayout>(R.id.add_advanced_options_container)
            if (advancedOptions.visibility == View.GONE) {
                advancedOptions.visibility = View.VISIBLE
            } else {
                advancedOptions.visibility = View.GONE
            }
        }

        // Setup add server dialog views.
        addServerLayout = layoutInflater.inflate(R.layout.add_server_dialog_content, null)
        serverName = addServerLayout.findViewById(R.id.server_name)
        serverIp = addServerLayout.findViewById(R.id.server_ip)
        serverPort = addServerLayout.findViewById(R.id.server_port)
        serverNickname = addServerLayout.findViewById(R.id.server_nickname)
        serverRealName = addServerLayout.findViewById(R.id.server_real_name)
        serverPassword = addServerLayout.findViewById(R.id.server_password)
        // Pretty much same as the advanced dropdown layout above.
        addServerLayout.findViewById<RelativeLayout>(R.id.server_advanced_options)!!.setOnClickListener {
            addServerLayout.findViewById<android.widget.ImageView>(com.opus.echo.R.id.server_advanced_dropdown)!!.rotation += 180f
            val advancedOptions =
                addServerLayout.findViewById<android.widget.LinearLayout>(com.opus.echo.R.id.server_advanced_options_container)
            if (advancedOptions.visibility == android.view.View.GONE) {
                advancedOptions.visibility = android.view.View.VISIBLE
            } else {
                advancedOptions.visibility = android.view.View.GONE
            }
        }
    }

    private fun onServerDialogAdd() {
        thread {
            serverDatabase?.addServer(
                Server(
                    serverName.text.toString(),
                    serverIp.text.toString(),
                    serverPort.text.toString().toInt(),
                    serverNickname.text.toString(),
                    serverRealName.text.toString(),
                    serverPassword.text.toString()
                )
            )
        }
    }

    companion object {
        const val TAG = "HomeActivity"
    }
}
