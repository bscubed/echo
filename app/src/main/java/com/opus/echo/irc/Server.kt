/*
 * Copyright (c) 2018 Brendan Szymanski
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.echo.irc

import androidx.room.Entity

/**
 * Here, we use name, address, and port as the primary keys to ensure two servers with the same characteristics
 * aren't added
 */
@Entity(tableName = "servers", primaryKeys = ["name", "address", "port"])
class Server(
    var name: String,
    var address: String,
    var port: Int,
    var nickname: String,
    var realName: String = "",
    var password: String = ""
)