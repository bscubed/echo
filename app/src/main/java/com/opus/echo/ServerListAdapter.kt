/*
 * Copyright (c) 2018 Brendan Szymanski
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.echo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.opus.echo.irc.Server
import kotlinx.android.synthetic.main.home_list_layout.view.*

class ServerListAdapter(private val context: HomeActivity, private val servers: MutableList<Server>) :
    RecyclerView.Adapter<ServerListAdapter.ServerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServerViewHolder {
        return ServerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.home_list_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = servers.size

    override fun onBindViewHolder(holder: ServerViewHolder, position: Int) {
        holder.title.text = servers[position].name

        // TODO: Replace this text with the last message sent in a conversation
        holder.message.text = servers[position].address

        if (holder.message.text.isBlank()) holder.message.visibility = View.GONE

        holder.menu.setOnClickListener {
            // TODO: Implement menu actions
        }

        holder.itemView.setOnClickListener {
            // TODO: Open the server conversation on click
        }
    }

    class ServerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.list_server_name
        var message: TextView = view.list_server_message
        var menu: ImageButton = view.list_server_menu
    }
}