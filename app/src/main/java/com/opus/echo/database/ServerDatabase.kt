/*
 * Copyright (c) 2018 Brendan Szymanski
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.echo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.opus.echo.irc.Server

@Database(entities = [Server::class], version = 1)
abstract class ServerDatabase : RoomDatabase() {

    val serverAddListeners: MutableList<ServerAddListener> = mutableListOf()

    abstract fun serverDao(): ServerHandler

    fun addServer(server: Server): List<Long> {
        val addedServerIds = serverDao().addServer(server)
        for (serverAddListener in serverAddListeners) {
            serverAddListener.onServerAdded(server)
        }
        return addedServerIds
    }

    interface ServerAddListener {
        fun onServerAdded(server: Server)
    }

    companion object {
        private var INSTANCE: ServerDatabase? = null

        /**
         * Returns an instance of our server database. If one doesn't exist, we create it.
         * @param context: Android application context
         * @return: The current database instance (Or the created instance, if one didn't exist)
         */
        fun getInstance(context: Context): ServerDatabase? {
            if (INSTANCE == null) {
                synchronized(ServerDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        ServerDatabase::class.java, "servers.db"
                    ).build()
                }
            }
            return INSTANCE
        }

        /**
         * Destroys the current instance, making retrieving information from the database impossible
         * without creating a new instance
         */
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}