/*
 * Copyright (c) 2018 Brendan Szymanski
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.echo.database

import androidx.room.*
import com.opus.echo.irc.Server


/**
 * This class handles all the SQL database queries needed to store and retrieve configured servers
 */
@Dao
interface ServerHandler {
    /**
     * Adds a server or list of servers and all of its information to the database
     * @param server: The server(s) to add to the database
     * @return: A list of IDs for rows that were successfully added
     */
    @Insert
    fun addServer(vararg server: Server): List<Long>

    /**
     * Updates the information for a server stored in the database.
     * If no server is found with a matching primary key, nothing happens and the return value will be zero
     * @param server: The server containing new information to store in the database
     * @return: The number of rows that were successfully updated
     */
    @Update
    fun updateServer(vararg server: Server): Int

    /**
     * Returns a list of all the servers saved in our database
     * @return: List of servers
     */
    @Query("SELECT * FROM servers")
    fun getServers(): MutableList<Server>

    /**
     * Deletes a server or list of servers stored in the database
     * @param servers: The server(s) to delete from the database
     * @return: The amount of rows successfully deleted from the database
     */
    @Delete
    fun deleteServer(vararg servers: Server): Int
}