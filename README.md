# Echo
Echo is a beautiful open-source IRC client for Android.
Written with :heart: in Kotlin with simplicity and efficiency in mind. 

As of right now there's not much to see, but feel free to stay tuned and watch this project grow into something beautiful. If you have a request, [submit one][request]!

### Features
  - [x] Elegant theme
  - [x] Loading saved servers from a local database
  - [ ] Add a new channel from a dialog pop-up
  - [ ] One-time configuration
  - [ ] Easily add popular servers with one click
  - [ ] Join popular channels with one click
  - [ ] Notification support
  - [ ] Advanced notification controls (mute specific channels, users, etc)
  - [ ] Dark mode, other theme controls
  
### Sneak Preview
<img src="assets/screenshot1.png" width="425"/> <img src="assets/screenshot2.png" width="425"/>

[request]: <https://gitlab.com/bscubed/echo/issues>